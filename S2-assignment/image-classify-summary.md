# Image Classification with Neural Networks

![ic11](/uploads/8e235fc2d77fe5b09ee52c5a730ae97e/ic11.jpg)

Image Classification Classification System consists of database that contains predefined patterns that compares with detected object to classify in to proper category.

**Different Image Processing Steps**:


* **Digital Data:** 

An image is captured by using digital camera or any mobile phone camera.

* **Pre-processing:** 

Improvement of the image data that surpresses the unwanted distortions and enhances the important image features.

![ic-1](/uploads/ae9ab75e9ca35e1a395d2c4be636287b/ic-1.jpg)

* **Feature extraction:** 

It is the process by which certain features of interest within an image sample are detected and represented for further processing.

![ic-2](/uploads/62908625ec2a4d3487af03d69159d409/ic-2.png)

* **Selection of training data:** 

Selection of the particular attribute which best describes the pattern. 

![ic-3](/uploads/29dbab1faa8e25d5074bd8417f72f986/ic-3.jpeg)

* **Decision and Classification:** 

Categorizes detected objects into predefined classes by using suitable method that compares the image patterns with the target patterns. Classification refers to the task of extracting information classes from a multiband raster image.


![ic-4](/uploads/f3b1b950de6db8adf8d6be4ba207731c/ic-4.png)



*  **Accuracy assessment:** An accuracy assessment is realized to identify possible sources of errors and as an indicator used in comparisons.
