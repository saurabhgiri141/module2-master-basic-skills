**Deep Neural Networks (DNNs)**

DNNs are typically Feed Forward Networks (FFNNs) in which data flows from the input layer to the output layer without going backward³ and the links between the layers are one way which is in the forward direction and they never touch a node again.

![dnn1](/uploads/092264515cd6aaf8c3745c51e0818de9/dnn1.jpeg)

The outputs are obtained by supervised learning with datasets of some information based on ‘what we want’ through back propagation. Like you go to a restaurant and the chef gives you an idea about the ingredients of your meal. FFNNs work in the same way as you will have the flavor of those specific ingredients while eating but just after finishing your meal you will forget what you have eaten. If the chef gives you the meal of same ingredients again you can’t recognize the ingredients, you have to start from scratch as you don’t have any memory of that. But the human brain doesn’t work like that.

**What is a Deep Neural Network?**

Nodes are little parts of the system, and they are like neurons of the human brain. When a stimulus hits them, a process takes place in these nodes. Some of them are connected and marked, and some are not, but in general, nodes are grouped into layers. 

The system must process layers of data between the input and output to solve a task. The more layers it has to process to get the result, the deeper the network is considered. There is a concept of Credit Assignment Path (CAP) which means the number of such layers needed for the system to complete the task. The neural network is deep if the CAP index is more than two.

![dnn12](/uploads/79bcd00ba33ccd76cf9df4bd0ac62c9e/dnn12.jpg)

A deep neural network is beneficial when you need to replace human labor with autonomous work without compromising its efficiency. The deep neural network usage can find various applications in real life. For example, a Chinese company Sensetime created a system of automatic face recognition system to identify criminals, which uses real-time cameras to find an offender in the crowd. Nowadays, it has become a popular practice in police and other governmental entities. 

The American company Pony.ai is another example of how you can use DNN. They developed a system for AI cars that can work without a driver. It requires more than just a simple algorithm of actions, but a much deeper learning system, which should be able to recognize people, road signs and other markings like trees, and other important objects. 

The famous company UbiTech creates AI robots. One of their creations is the Alpha 2 robot that can live in a family, speak with its members, search for information, write messages, and execute  voice commands.

**What is the Difference Between the Neural Network and Deep Neural Network?**

![dnn3](/uploads/04549d846d7ebddc469bb275f7754d22/dnn3.png)

You can compare a neural network to a chess game with a computer. It has algorithms, according to which it determines tactics, depending on your moves and actions. The programmer enters data on how each figure moves into the computer’s database, determines the boundaries of the chessboard, introduces a huge number of strategies that chess players play by. At the same time, the computer may, for example, be able to learn from you and other people, and it can become a deep neural network. In a while, playing with different players, it can become invincible.

The neural network is not a creative system, but a deep neural network is much more complicated than the first one. It can recognize voice commands, recognize sound and graphics, do an expert review, and perform a lot of other actions that require prediction, creative thinking, and analytics. Only the human brain has such possibilities. The neural network can get one result (a word, an action, a number, or a solution), while the deep neural network solves the problem more globally and can draw conclusions or predictions depending on the information supplied and the desired result. The neural network requires a specific input of data and algorithms of solutions, and the deep neural network can solve a problem without a significant amount of marked data.

**What Is Deep Learning Neural Network?**

The neural network needs to learn all the time to solve tasks in a more qualified manner or even to use various methods to provide a better result. When it gets new information in the system, it learns how to act accordingly to a new situation.

Learning becomes deeper when tasks you solve get harder. Deep neural network represents the type of machine learning when the system uses many layers of nodes to derive high-level functions from input information. It means transforming the data into a more creative and abstract component.

In order to understand the result of deep learning better, let's imagine a picture of an average man. Although you have never seen this picture and his face and body before, you will always identify that it is a human and differentiate it from other creatures. This is an example of how the deep neural network works. Creative and analytical components of information are analyzed and grouped to ensure that the object is identified correctly. These components are not brought to the system directly, thus the ML system has to modify and derive them. 

**What is a Deep Convolutional Neural Network?**

There are different types of neural networks and the differences between them lies in their work principles, the scheme of actions, and the application areas. Convolutional neural networks (CNN) are mostly used for image recognition, and rarely for audio recognition. It is mostly applied to images because there is no need to check all the pixels one by one. CNN checks an image by blocks, starting from the left upper corner and moving further pixel by pixel up to a successful completion. Then the result of every verification is passed through a convolutional layer, where data elements have connections while others don’t. Based on this data, the system can produce the result of the verifications and can conclude what is in the picture. 


